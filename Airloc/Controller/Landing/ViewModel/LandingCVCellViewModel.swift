//
//  LandingCVCellViewModel.swift
//  Geofence
//
//  Created by Wan Rahafiz Wan Abdul Rahim on 23/10/2019.
//  Copyright © 2019 Wan Rahafiz Wan Abdul Rahim. All rights reserved.
//

import Foundation

protocol LandingCVCellViewModelInputs {
    
}

protocol LandingCVCellViewModelOutputs {
    
    var animationName: Box<String?> { get }
    var description: Box<String?> { get }
    
}

protocol LandingCVCellViewModelType {
    var inputs: LandingCVCellViewModelInputs { get }
    var outputs: LandingCVCellViewModelOutputs { get }
}

class LandingCVCellViewModel: LandingCVCellViewModelInputs, LandingCVCellViewModelOutputs, LandingCVCellViewModelType {
    
    let animationName: Box<String?> = Box(nil)
    let description: Box<String?> = Box(nil)
    
    var inputs: LandingCVCellViewModelInputs { return self }
    var outputs: LandingCVCellViewModelOutputs { return self }
    
    init() {
        
    }
    
}
