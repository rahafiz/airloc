//
//  HomeVC.swift
//  Airloc
//
//  Created by Wan Rahafiz Wan Abdul Rahim on 27/02/2020.
//  Copyright © 2020 wanrahafiz. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class HomeVC: UIViewController {

    
    @IBOutlet weak var mapView: MKMapView!
    var locationManager = CLLocationManager()
    private var currentCoordinate: CLLocationCoordinate2D?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        mapView.delegate = self
        createAirports()
        configureLocationServices()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        
    }

    private func configureLocationServices() {
        locationManager.delegate = self
        if LocationManager.userEnableLocationService() {
            
            if LocationManager.locationAuthorizationStatus() {
                locationManager.requestWhenInUseAuthorization()
            }
            
            beginLocationUpdates(locationManager: locationManager)

        }
    }
    
    private func beginLocationUpdates(locationManager: CLLocationManager) {
        mapView.showsUserLocation = true
        mapView.isScrollEnabled = true
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()

    }
    
    private func createAirports() {
        let klia1: [String: Any] = ["title": "KLIA", "latitude": 2.754679, "longitude": 101.704517]
        let klia2: [String: Any] = ["title": "KLIA 2", "latitude": 2.7432, "longitude": 101.6864]
        let lcct: [String: Any] = ["title": "LCCT", "latitude": 2.7406, "longitude": 101.7195]
        let subang: [String: Any] = ["title": "Subang Airport", "latitude": 3.1263, "longitude": 101.5531]

        let airports = [klia1, klia2, lcct, subang]
        
        Cache.setAirports(value: airports, keyName: "airports")

    }
    
    private func zoomToLatestLocation(with cordinate: CLLocationCoordinate2D) {
        let zoomRegion = MKCoordinateRegion(center: cordinate, latitudinalMeters: 10000, longitudinalMeters: 10000)
        mapView.setRegion(zoomRegion, animated: true)

    }
    
    private func userDistance(from point: MKPointAnnotation, currentLocation: CLLocation?) -> Double? {
        guard let userLocation = currentLocation else {
            return nil // User location unknown!
        }
        let pointLocation = CLLocation(
            latitude:  point.coordinate.latitude,
            longitude: point.coordinate.longitude
        )
        return userLocation.distance(from: pointLocation)
    }

    
    private func addAnnotations(currentLocation: CLLocation) {
        guard let airports = Cache.getAirports(keyName: "airports") else { return }
        
        for airport in airports {
            let annotation = MKPointAnnotation()
            let airportLatitude = airport["latitude"] as? Double ?? 0.0
            let airportLongitude = airport["longitude"] as? Double ?? 0.0
            let airportTitle = airport["title"] as? String
            annotation.coordinate = CLLocationCoordinate2D(latitude: airportLatitude, longitude: airportLongitude)
            annotation.title = airportTitle

            if let distance = userDistance(from: annotation, currentLocation: currentLocation) {
                // Use distance here...
                let finalDistance = distance/1000
                let airportSubtitle = String(format: "%.3f km", finalDistance)
                annotation.subtitle = airportSubtitle

            }

            mapView.addAnnotation(annotation)
        }
    }
    
    func goToDetailPage(title: String?, subtitle: String?) {
        
        let identifier = String(describing: AirportDetailsVC.self)
        guard let vc = UIViewController.vc(
            "Main",
            identifier: identifier
        ) as? AirportDetailsVC else { return }
        
        vc.airportTitle = title
        vc.airportSubtitle = subtitle        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension HomeVC: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    
        guard let currentLocation = locations.first else { return }
        if currentCoordinate == nil {
            zoomToLatestLocation(with: currentLocation.coordinate)
            addAnnotations(currentLocation: currentLocation)
        }
        
        currentCoordinate = currentLocation.coordinate
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
    }
}

extension HomeVC: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard let title = view.annotation?.title else { return }
        guard let subtitle = view.annotation?.subtitle else { return }

        
        goToDetailPage(title: title, subtitle: subtitle)
    }
    
    /*func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "AnnotationView")
        
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "AnnotationView")
        }
        
        annotationView?.image = UIImage(named: "ic_airport")
        
        return annotationView
    }*/
}
