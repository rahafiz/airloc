//
//  ThemeManager.swift
//  Geofence
//
//  Created by Wan Rahafiz Wan Abdul Rahim on 28/10/2019.
//  Copyright © 2019 Wan Rahafiz Wan Abdul Rahim. All rights reserved.
//

import UIKit

enum ThemeBackgroundStyle: Int {
    case `default`      //
    case lightContent   // For use on dark background
}

enum TextStyle: Int {
    case bst_t1
    case bst_t2
    case bst_st1
    case bst_st2
    case bst_st3
    case bst_b1
    case bst_b2
    case bst_b3
    case bst_b1_bold
    case bst_b2_bold
    case bst_b3_bold
    case bst_alert1
    case bst_alert2
    case bst_alert3
    case bst_btn_action
    case bst_btn_action_secondary
    case bst_hint1
    case bst_hint2
    case title
    case content
    case contentHighlighted
    case contentLink
    case inputTitle
    case inputTxtField
    case popUpTitle
    case popUpHeaderTitle
    case popUpContent
    case popUpContentHighlighted
    case popUpContentLink
    case tipPopUpTitle
    case tipPopUpContent
    case t1
    case t2
    case t3
    case t4
    case t5
    case t6
    case t7
    case t8
    case t9
    case t10
    case t11
    case t12
    case t13
    case t14
    case t15
    case t16
    case t17
    case t18
    case t19
    case t20
}

class ThemeManager: NSObject {
    struct Metrics {
        static let MinimumSectionHeight: CGFloat = {
            if #available(iOS 9.0, *) {
                return CGFloat.leastNonzeroMagnitude
            } else {
                return 1.1
            }
        }()
        
        static func sideMargin() -> CGFloat {
            return 24.0
        }
    }
    
    // MARK: - Shadow
    static func addDefaultShadow(_ view: UIView, cornerRadius: CGFloat? = 8.0) {
        view.layer.shadowColor = UIColor(red: 120/255, green: 120/255, blue: 120/255, alpha: 0.2).cgColor
        view.layer.shadowRadius = 2
        view.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        view.layer.shadowOpacity = 1
        view.layer.shouldRasterize = true
        view.layer.rasterizationScale = UIScreen.main.scale
        
        if let cornerRadius = cornerRadius {
            view.layer.cornerRadius = cornerRadius
            view.clipsToBounds = true
        }
    }
    
    static func applyShadow(view: UIView,
                            shadowOffset: CGSize = CGSize.zero,
                            shadowColor: CGColor = UIColor.darkGray.cgColor,
                            shadowOpacity: Float = 0.6,
                            shadowRadius: CGFloat? = 5.0) {
        view.layer.shadowOffset = shadowOffset
        view.layer.shadowColor = shadowColor
        view.layer.shadowOpacity = shadowOpacity
        view.layer.shadowRadius = shadowRadius ?? view.layer.shadowRadius
    }
    
    static func addLineSpacingString(text: String, lineSpacing: CGFloat = 5, fontType: UIFont, textColor: UIColor?) -> NSAttributedString? {
        if text == "" {
            return nil
        }

        let attributedString = NSMutableAttributedString(string: text)
        
        // *** Create instance of `NSMutableParagraphStyle`
        let paragraphStyle = NSMutableParagraphStyle()
        
        // *** set LineSpacing property in points ***
        paragraphStyle.lineSpacing = lineSpacing // Whatever line spacing you want in points
        
        // *** Apply attribute to string ***
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, attributedString.length))
        attributedString.addAttribute(NSAttributedString.Key.font,
                             value: fontType,
                             range: NSMakeRange(0, attributedString.length))
        
        if let _textColor = textColor {
            attributedString.addAttribute(
                NSAttributedString.Key.foregroundColor,
                value: _textColor,
                range: NSMakeRange(0, attributedString.length)
            )
        }
        
        return attributedString
    }
    
    // NOTE: Don't rely on this method too much. Converting HTML to AttributedString should only be done for complex cases
    // This is a performance intensive function and its usage should be minimized.
    // MARK: - HTML attributed string transformer
    static func htmlAttributedString(text: String?,
                                     alignment: NSTextAlignment = .left,
                                     regularFont: UIFont,
                                     boldFont: UIFont,
                                     textColor: UIColor?,
                                     lineSpacing: CGFloat? = nil) -> NSAttributedString? {
        do {
            guard let t = text else { return nil }
            guard t.count > 0 else { return nil }
            
            let replacedText = t.replacingOccurrences(of: "\n", with: "<br>")
            guard let data = replacedText.data(using: String.Encoding.unicode) else { return nil }
            
            let options: [NSAttributedString.DocumentReadingOptionKey : Any] = [
                NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html
            ]
            
            let attrStr = try NSMutableAttributedString.init(
                data: data,
                options: options,
                documentAttributes: nil
            )
            
            if alignment != .left && lineSpacing == nil {
                if let paragraphStyle = NSParagraphStyle.default.mutableCopy() as? NSMutableParagraphStyle {
                    paragraphStyle.alignment = alignment
                    paragraphStyle.lineSpacing = 6
                    
                    attrStr.addAttributes(
                        [NSAttributedString.Key.paragraphStyle: paragraphStyle],
                        range: NSRange.init(location: 0, length: attrStr.length)
                    )
                }
            }
            
            if let lineSpacing = lineSpacing {
                // *** Create instance of `NSMutableParagraphStyle`
                let paragraphStyle = NSMutableParagraphStyle()
                
                // *** set LineSpacing property in points ***
                paragraphStyle.lineSpacing = lineSpacing // Whatever line spacing you want in points
                
                // *** Apply attribute to string ***
                attrStr.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, attrStr.length))
            }
            
            attrStr.beginEditing()
            attrStr.enumerateAttribute(NSAttributedString.Key.font, in: NSMakeRange(0, attrStr.length), options: .init(rawValue: 0)) {
                (value, range, _) in
                if let font = value as? UIFont {
                    if let _ = font.fontName.lowercased().range(of: "bold") {
                        attrStr.addAttribute(NSAttributedString.Key.font,
                                             value: boldFont,
                                             range: range)
                    } else {
                        attrStr.addAttribute(NSAttributedString.Key.font,
                                             value: regularFont,
                                             range: range)
                    }
                    
                    if let _textColor = textColor {
                        attrStr.addAttribute(
                            NSAttributedString.Key.foregroundColor,
                            value: _textColor,
                            range: range
                        )
                    }
                }
            }
            
            attrStr.endEditing()
            
            return attrStr
        } catch let error {
           // BSTLogger.shared.debug("ThemeManager.htmlAttributedString: \(error.localizedDescription)")
        }
        
        return nil
    }
    
    // MARK: - UINavigationBarButton
    static func defaultBackBarButton(barStyle: UIStatusBarStyle = .default, target: AnyObject?, action: Selector?) -> UIBarButtonItem {
        let img: UIImage? = {
            switch barStyle {
            case .lightContent:
                return UIImage(named: "back-white")?
                    .withRenderingMode(.alwaysOriginal)
            case .default:
                return UIImage(named: "back")?
                    .withRenderingMode(.alwaysOriginal)
            case .blackOpaque:
                return UIImage(named: "back-white")?
                    .withRenderingMode(.alwaysOriginal)
            case .darkContent:
                return UIImage(named: "back")?
                    .withRenderingMode(.alwaysOriginal)
            @unknown default:
                return UIImage(named: "back")?
                    .withRenderingMode(.alwaysOriginal)

            }
        }()
        let backBtn = UIBarButtonItem(
            image: img,
            style: .plain,
            target: target,
            action: action
        )
        
        return backBtn
    }
    
    static func defaultCloseBarButton(barStyle: UIStatusBarStyle = .default, target: AnyObject?, action: Selector?) -> UIBarButtonItem {
        let img: UIImage? = {
            switch barStyle {
            case .lightContent:
                return UIImage(named: "close")?
                    .withRenderingMode(.alwaysTemplate)
            case .default:
                return UIImage(named: "close")?
                    .withRenderingMode(.alwaysOriginal)
            case .blackOpaque:
                return UIImage(named: "close")?
                    .withRenderingMode(.alwaysOriginal)
            case .darkContent:
                 return UIImage(named: "close")?
                    .withRenderingMode(.alwaysOriginal)
            @unknown default:
                 return UIImage(named: "close")?
                    .withRenderingMode(.alwaysOriginal)
            }
        }()
        let backBtn = UIBarButtonItem(
            image: img,
            style: .plain,
            target: target,
            action: action
        )
        backBtn.tintColor = UIColor.white
        
        return backBtn
    }
    
    // MARK: - Labels
    static func apply(label: UILabel!,
                      style: TextStyle,
                      backgroundStyle: ThemeBackgroundStyle = .default) {
        apply(label: label,
              fontStyle: style,
              colorStyle: style,
              backgroundStyle: backgroundStyle)
    }
    
    static func apply(label: UILabel!,
                      fontStyle: TextStyle,
                      colorStyle: TextStyle,
                      backgroundStyle: ThemeBackgroundStyle = .default) {
        label.font = fontForStyle(
            style: fontStyle
        )
        
        label.textColor = colorForStyle(
            style: colorStyle,
            backgroundStyle: backgroundStyle
        )
    }
    
    // MARK: - TextFields
    static func apply(txtField: UITextField!,
                      style: TextStyle,
                      backgroundStyle: ThemeBackgroundStyle = .default) {
        apply(txtField: txtField,
              fontStyle: style,
              colorStyle: style,
              backgroundStyle: backgroundStyle)
    }
    
    static func apply(txtField: UITextField!,
                      fontStyle: TextStyle,
                      colorStyle: TextStyle,
                      backgroundStyle: ThemeBackgroundStyle) {
        txtField.font = fontForStyle(
            style: fontStyle
        )
        
        txtField.textColor = colorForStyle(
            style: colorStyle,
            backgroundStyle: backgroundStyle
        )
    }
    
    // MARK: - Buttons
    static func applyBorder(btn: UIButton,
                            cornerRadius: CGFloat? = nil,
                            borderColor: UIColor? = nil,
                            borderWidth: CGFloat? = nil) {
        let cornerRadius = cornerRadius ?? btn.frame.height / 2.0
        let borderColor =  borderColor ?? btn.currentTitleColor
        let borderWidth = borderWidth ?? 1.0
    
        btn.layer.borderColor = borderColor.cgColor
        btn.layer.borderWidth = borderWidth
        btn.layer.cornerRadius = cornerRadius
        btn.clipsToBounds = true
    }
    
    static func apply(btn: UIButton,
                      style: TextStyle,
                      backgroundStyle: ThemeBackgroundStyle = .default,
                      for state: UIControl.State = .normal) {
        apply(btn: btn,
              fontStyle: style,
              colorStyle: style,
              backgroundStyle: backgroundStyle,
              for: state)
    }
    
    static func apply(btn: UIButton,
                      fontStyle: TextStyle,
                      colorStyle: TextStyle,
                      backgroundStyle: ThemeBackgroundStyle = .default,
                      for state: UIControl.State = .normal) {
        btn.titleLabel?.font = fontForStyle(
            style: fontStyle
        )
        
        btn.setTitleColor(
            colorForStyle(
                style: colorStyle,
                backgroundStyle: backgroundStyle
            ),
            for: state
        )
    }
    
    // MARK: - Toolbars
    static func doneToolbar(text: String?, target: Any?, action: Selector?) -> UIToolbar {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 0, height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let doneTxt = text ?? "Done"
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: doneTxt, style: UIBarButtonItem.Style.done, target: target, action: action)
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        
        return doneToolbar
    }
    
    // MARK: - Styles
    static func dictionaryFor(style: TextStyle) -> [NSAttributedString.Key: Any] {
        return dictionaryFor(fontStyle: style, colorStyle: style)
    }

    static func dictionaryFor(fontStyle: TextStyle, colorStyle: TextStyle) -> [NSAttributedString.Key: Any] {
        var dictionary: [NSAttributedString.Key: Any] = [:]
        
        dictionary[NSAttributedString.Key.font] = fontForStyle(
            style: fontStyle
        )
        
        dictionary[NSAttributedString.Key.foregroundColor] = colorForStyle(
            style: colorStyle
        )
        
        return dictionary
    }

    static func dictionaryFor(font: UIFont?, color: UIColor?) -> [NSAttributedString.Key: Any] {
        var dictionary: [NSAttributedString.Key: Any] = [:]
        
        if let _font = font {
            dictionary[NSAttributedString.Key.font] = _font
        }
        
        if let _color = color {
            dictionary[NSAttributedString.Key.foregroundColor] = _color
        }
        
        return dictionary

    }
    
    static func fontForStyle(style: TextStyle) -> UIFont {
        var font: UIFont
        
        switch style {
        case .bst_t1:
            font = UIFont.bst_t1()
        case .bst_t2:
            font = UIFont.bst_t2()
        case .bst_st1:
            font = UIFont.bst_st1()
        case .bst_st2:
            font = UIFont.bst_st2()
        case .bst_st3:
            font = UIFont.bst_st3()
        case .bst_b1:
            font = UIFont.bst_b1()
        case .bst_b2:
            font = UIFont.bst_b2()
        case .bst_b3:
            font = UIFont.bst_b3()
        case .bst_b1_bold:
            font = UIFont.bst_b1_bold()
        case .bst_b2_bold:
            font = UIFont.bst_b2_bold()
        case .bst_b3_bold:
            font = UIFont.bst_b3_bold()
        case .bst_alert1:
            font = UIFont.bst_alert1()
        case .bst_alert2:
            font = UIFont.bst_alert2()
        case .bst_alert3:
            font = UIFont.bst_alert3()
        case .bst_btn_action:
            font = UIFont.bst_btn_action()
        case .bst_btn_action_secondary:
            font = UIFont.bst_btn_action_secondary()
        case .bst_hint1:
            font = UIFont.bst_hint1()
        case .bst_hint2:
            font = UIFont.bst_hint2()
        case .title:
            font = UIFont.bst_t1()
        case .content:
            font = UIFont.t16()
        case .contentHighlighted, .contentLink:
            font = UIFont.t1()
        case .inputTitle:
            font = UIFont.t16()
        case .inputTxtField:
            font = UIFont.t16()
        case .popUpTitle:
            font = UIFont.t5()
        case .popUpHeaderTitle:
            font = UIFont.t12()
        case .popUpContent:
            font = UIFont.t15()
        case .popUpContentHighlighted, .popUpContentLink:
            font = UIFont.t1()
        case .tipPopUpTitle:
            font = UIFont.t18()
        case .tipPopUpContent:
            font = UIFont.t19()
        case .t1:
            font = UIFont.t1()
        case .t2:
            font = UIFont.t2()
        case .t3:
            font = UIFont.t3()
        case .t4:
            font = UIFont.t4()
        case .t5:
            font = UIFont.t5()
        case .t6:
            font = UIFont.t6()
        case .t7:
            font = UIFont.t7()
        case .t8:
            font = UIFont.t8()
        case .t9:
            font = UIFont.t9()
        case .t10:
            font = UIFont.t10()
        case .t11:
            font = UIFont.t11()
        case .t12:
            font = UIFont.t12()
        case .t13:
            font = UIFont.t13()
        case .t14:
            font = UIFont.t14()
        case .t15:
            font = UIFont.t15()
        case .t16:
            font = UIFont.t16()
        case .t17:
            font = UIFont.t17()
        case .t18:
            font = UIFont.t18()
        case .t19:
            font = UIFont.t19()
        case .t20:
            font = UIFont.t20()
        }
        
        return font
    }
    
    static func colorForStyle(style: TextStyle, backgroundStyle: ThemeBackgroundStyle = .default) -> UIColor {
        var color: UIColor
        
        switch style {
        case .bst_t1, .bst_t2: do {
            switch backgroundStyle {
            case .lightContent:
                color = UIColor.white
            case .default:
                color = UIColor.titleBlack()
            }
        }
        case .bst_st1, .bst_st2, .bst_st3: do {
            switch backgroundStyle {
            case .lightContent:
                color = UIColor.white
            case .default:
                color = UIColor.contentBlack()
            }
        }
        case .bst_b1, .bst_b2, .bst_b3, .bst_b1_bold, .bst_b2_bold, .bst_b3_bold: do {
            switch backgroundStyle {
            case .lightContent:
                color = UIColor.white
            case .default:
                color = UIColor.contentBlack()
            }
        }
        case .bst_alert1, .bst_alert2, .bst_alert3: do {
            switch backgroundStyle {
            case .lightContent:
                color = UIColor.white
            case .default:
                color = UIColor.primaryRed()
            }
        }
        case .bst_btn_action: do {
            switch backgroundStyle {
            case .lightContent:
                color = UIColor.white
            case .default:
                color = UIColor.primaryRed()
            }
        }
        case .bst_btn_action_secondary: do {
            switch backgroundStyle {
            case .lightContent:
                color = UIColor.white
            case .default:
                color = UIColor.contentBlack()
            }
        }
        case .bst_hint1, .bst_hint2: do {
            switch backgroundStyle {
            case .lightContent:
                color = UIColor.white
            case .default:
                color = UIColor.placeholderColor()
            }
        }
        case .title:
            color = UIColor.titleBlack()
        case .content, .contentHighlighted:
            color = UIColor.contentBlack()
        case .contentLink:
            color = UIColor.primaryRed()
        case .inputTitle:
            color = UIColor.titleBlack()
        case .inputTxtField:
            color = UIColor.titleBlack()
        case .popUpTitle:
            color = UIColor.gray
        case .popUpHeaderTitle:
            color = UIColor.black
        case .popUpContent, .popUpContentHighlighted:
            color = UIColor.contentBlack()
        case .popUpContentLink:
            color = UIColor.primaryRed()
        case .tipPopUpTitle, .tipPopUpContent:
            color = UIColor.white
        case .t1, .t2, .t3, .t4, .t5, .t6, .t7, .t8, .t9, .t10,
             .t11, .t12, .t13, .t14, .t15, .t16, .t17, .t18, .t19, .t20:
            color = UIColor.titleBlack()
        }
        
        return color
    }
    
    // MARK: - Functions
    static func printFonts() {
        let fontFamilyNames = UIFont.familyNames
        for familyName in fontFamilyNames {
            //BSTLogger.shared.debug("------------------------------")
            //BSTLogger.shared.debug("Font Family Name = [\(familyName)]")
            //let names = UIFont.fontNames(forFamilyName: familyName)
            //BSTLogger.shared.debug("Font Names = [\(names)]")
        }
    }
}

struct PriceFormatter {
    static let defaultFormatter: NumberFormatter = {
        let formatter: NumberFormatter = NumberFormatter()
        
        formatter.numberStyle = .currency
        formatter.currencyCode = "MYR"
        formatter.currencySymbol = "RM"
        
        return formatter
    }()
    
    static func format(amount: Double ,
                       currencyCode: String = "MYR",
                       currencySymbol: String = "RM ",
                       locale: NSLocale = NSLocale.init(localeIdentifier: "en_US"),
                       maxFractionDigits: Int = 2,
                       alwaysShowDecimal: Bool = false) -> String? {
        let formatter: NumberFormatter = defaultFormatter.copy() as! NumberFormatter
        let amountNumber = NSDecimalNumber.init(value: amount)
        let flagZeroFractionDigits: Bool = {
            return amountNumber.doubleValue.truncatingRemainder(dividingBy: 1.0) == 0
        }()
        
        formatter.currencyCode = currencyCode
        formatter.currencySymbol = currencySymbol
        formatter.maximumFractionDigits = alwaysShowDecimal ? maxFractionDigits : (flagZeroFractionDigits ? 0 : maxFractionDigits)
        
        return formatter.string(from: amountNumber)
    }
    
    static func format(amount: String,
                       currencyCode: String = "MYR",
                       currencySymbol: String = "RM ",
                       locale: NSLocale = NSLocale.init(localeIdentifier: "en_US"),
                       maxFractionDigits: Int = 2,
                       alwaysShowDecimal: Bool = false) -> String? {
        let formatter: NumberFormatter = defaultFormatter.copy() as! NumberFormatter
        let amountNumber = NSDecimalNumber.init(string: amount,
                                                locale: locale)
        let flagZeroFractionDigits: Bool = {
            return amountNumber.doubleValue.truncatingRemainder(dividingBy: 1.0) == 0
        }()
        
        
        formatter.currencyCode = currencyCode
        formatter.currencySymbol = currencySymbol
        formatter.maximumFractionDigits = alwaysShowDecimal ? maxFractionDigits : (flagZeroFractionDigits ? 0 : maxFractionDigits)
        
        return formatter.string(from: amountNumber)
    }
    
    static func format(amount: Int,
                       offset: Int = 2,
                       currencySymbol: String = "RM ",
                       maxFractionDigits: Int = 2,
                       alwaysShowDecimal: Bool = false) -> String? {
        let formatter: NumberFormatter = defaultFormatter.copy() as! NumberFormatter
        let offsetDivider = NSDecimalNumber.init(value: pow(10, Double(offset)))
        let offsetAmount = NSDecimalNumber.init(value: amount).dividing(by: offsetDivider)
        let flagZeroFractionDigits: Bool = {
            return offsetAmount.doubleValue.truncatingRemainder(dividingBy: 1.0) == 0
        }()
        
        formatter.currencySymbol = currencySymbol
        formatter.maximumFractionDigits = alwaysShowDecimal ? maxFractionDigits : (flagZeroFractionDigits ? 0 : maxFractionDigits)
        
        return formatter.string(from: offsetAmount)
    }
}

struct MsisdnFormatter {
    static let numberPrefixes: [String] = [
        "+60 ",
        "+60",
        "60",
        "0"
    ]
    static let malaysiaNumberPrefix = "+60 "
    
    /*static func format(msisdn: String,
                       withPrefix: Bool = false) -> String {
        var formattedMsisdn: String = msisdn.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).replacingOccurrences(
            of: " ",
            with: ""
        ).digits
        
        for prefix in numberPrefixes {
            if let rangeOfPrefix = formattedMsisdn.range(
                of: prefix,
                options: .anchored) {
                    formattedMsisdn.removeSubrange(rangeOfPrefix)
                break
            }
        }
        
        if withPrefix {
            formattedMsisdn.insert(
                contentsOf: malaysiaNumberPrefix,
                at: formattedMsisdn.startIndex
            )
        }
        
        return formattedMsisdn
    }*/
}

extension DateFormatter {
    static let defaultFormatter: DateFormatter = {
        let formatter: DateFormatter = DateFormatter()
        
        formatter.dateFormat = "d MMM, h:mm a"
        
        return formatter
    }()
    
    static let dateStartTimeFormatter: DateFormatter = {
        let formatter: DateFormatter = DateFormatter()
        
        formatter.dateFormat = "d LLL, h.mm a '-' "
        
        return formatter
    }()
    
    static let endTimeFormatter: DateFormatter = {
        let formatter: DateFormatter = DateFormatter()
        
        formatter.dateFormat = "h.mm a"
        
        return formatter
    }()
    
    static func format(date: Date) -> String {
        return defaultFormatter.string(from: date)
    }
    
    static func formatDay(day: Int) -> String {
        var format: String
        
        if day > 1 {
            format = "%1zd days"
        } else {
            format = "%1zd day"
        }
        
        return String.init(format: format, day)
    }
}

extension UIColor {
    // https://gist.github.com/delputnam/2d80e7b4bd9363fd221d131e4cfdbd8f
    func isLight() -> Bool {
        // algorithm from: http://www.w3.org/WAI/ER/WD-AERT/#color-contrast
        
        if let components = self.cgColor.components {
            let brightness = ((components[0] * 299) + (components[1] * 587) + (components[2] * 114)) / 1000
            
            if brightness < 0.5 {
                return false
            } else {
                return true
            }
        }
        
        return false
    }
}


