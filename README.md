# Airloc

This repository shows how use MapKit to set multiple annotation for nearby airport on your current location.

# How to run the project

After cloning the project, you need to run pod install before build the app. 
Then, go to > Pods > Builds Settings > Swift Compiler - Language > Swift Language Version > Swift 4.2 (for "SwiftyJSON" and "UIColor_Hex_Swift" pods)

