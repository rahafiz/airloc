//
//  UIView+Animation.swift
//  hubhome-ios
//
//  Created by Wan Rahafiz Wan Abdul Rahim on 17/01/2020.
//  Copyright © 2020 UEM Sunrise. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func fadeIn(duration: TimeInterval = 0.8, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in }) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveLinear, animations: {
            self.alpha = 0.0
        }, completion: completion)
    }
    
    func fadeOut(duration: TimeInterval = 0.8, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in }) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveLinear, animations: {
            self.alpha = 1.0
        }, completion: completion)
    }
    
    func scaleOut(duration: TimeInterval = 0.8, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in }) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            self.alpha = 0.0
        }, completion: completion)
    }
    
    func slideIn(duration: TimeInterval = 0.8, completionDelegate: CAAnimationDelegate? = nil) {
        let slideInFromRight = CATransition()
        if let delegate = completionDelegate {
            slideInFromRight.delegate = delegate
        }
        
        slideInFromRight.type = CATransitionType.push
        slideInFromRight.subtype = CATransitionSubtype.fromRight
        slideInFromRight.duration = duration
        slideInFromRight.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        slideInFromRight.fillMode = CAMediaTimingFillMode.removed
        self.layer.add(slideInFromRight, forKey: "slideInFromRight")
    }
    
    func pushUp() {
        
        let toPoint: CGPoint = CGPoint(x: 0.0, y: -70.0)
        let fromPoint: CGPoint = CGPoint.zero
        
        let moveUp = CABasicAnimation(keyPath: "position")
        moveUp.isAdditive = true
        moveUp.fromValue = NSValue(cgPoint: fromPoint)
        moveUp.toValue = NSValue(cgPoint: toPoint)
        moveUp.fillMode = CAMediaTimingFillMode.forwards
        moveUp.isRemovedOnCompletion = false
        moveUp.duration = 0.5
        self.layer.add(moveUp, forKey: "move")
    }
    


}

extension UIView {
    var snapshot: UIImage? {
        UIGraphicsBeginImageContext(self.frame.size)
        guard let context = UIGraphicsGetCurrentContext() else {
            return nil
        }
        layer.render(in: context)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}
