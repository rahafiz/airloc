//
//  LocationManagerHelper.swift
//  Airloc
//
//  Created by Wan Rahafiz Wan Abdul Rahim on 27/02/2020.
//  Copyright © 2020 wanrahafiz. All rights reserved.
//

import Foundation
import CoreLocation

class LocationManager: CLLocationManager {
    
    static func userEnableLocationService() -> Bool {
        if CLLocationManager.locationServicesEnabled() {
            return true
        } else {
            return false
        }
    }
    
    static func locationAuthorizationStatus() -> Bool {
    
        if CLLocationManager.authorizationStatus() == .restricted ||
            CLLocationManager.authorizationStatus() == .denied ||
            CLLocationManager.authorizationStatus() == .notDetermined {
            return true
        } else {
            return false
        }
    }
    
}
