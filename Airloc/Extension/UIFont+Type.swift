//
//  UIFont+Type.swift
//  hubhome-ios
//
//  Created by Wan Rahafiz Wan Abdul Rahim on 17/01/2020.
//  Copyright © 2020 UEM Sunrise. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    
    // Title
    static func bst_t1() -> UIFont {
        return UIFont.ralewayExtraBold(size: 24)
    }
    
    static func bst_t2() -> UIFont {
        return UIFont.ralewayExtraBold(size: 20)
    }
    
    // SubTitle
    static func bst_st1() -> UIFont {
        return UIFont.ralewayBold(size: 16)
    }
    
    static func bst_st2() -> UIFont {
        return UIFont.ralewayBold(size: 14)
    }
    
    static func bst_st3() -> UIFont {
        return UIFont.ralewayBold(size: 12)
    }
    
    // Body
    static func bst_b1() -> UIFont {
        return UIFont.raleway(size: 14)
    }
    
    static func bst_b2() -> UIFont {
        return UIFont.raleway(size: 12)
    }
    
    static func bst_b3() -> UIFont {
        return UIFont.raleway(size: 10)
    }
    
    // Body Bold
    static func bst_b1_bold() -> UIFont {
        return UIFont.ralewayBold(size: 14)
    }
    
    static func bst_b2_bold() -> UIFont {
        return UIFont.ralewayBold(size: 12)
    }
    
    static func bst_b3_bold() -> UIFont {
        return UIFont.ralewayBold(size: 10)
    }
    
    // Alert
    static func bst_alert1() -> UIFont {
        return UIFont.ralewayBold(size: 16)
    }
    
    static func bst_alert2() -> UIFont {
        return UIFont.ralewayBold(size: 14)
    }
    
    static func bst_alert3() -> UIFont {
        return UIFont.ralewayBold(size: 12)
    }
    
    //
    static func bst_contact_title() -> UIFont {
        return UIFont.ralewayBold(size: 16)
    }
    
    static func bst_contact_number() -> UIFont {
        return UIFont.raleway(size: 12)
    }
    
    static func bst_contact_shortname() -> UIFont {
        return UIFont.ralewaySemiBold(size: 14)
    }
    
    //
    static func bst_btn_action() -> UIFont {
        return UIFont.ralewayBold(size: 14)
    }
    
    static func bst_btn_action_secondary() -> UIFont {
        return UIFont.ralewayBold(size: 14)
    }
    
    //
    static func bst_hint1() -> UIFont {
        return UIFont.ralewayMedium(size: 14)
    }
    
    static func bst_hint2() -> UIFont {
        return UIFont.ralewayMedium(size: 12)
    }
    
    //
    static func bst_pin_popup_price() -> UIFont {
        return UIFont.ralewayExtraBold(size: 32)
    }
    
    //
    static func t1() -> UIFont {
        return UIFont(name: "Raleway-Bold", size: 17)!
    }
    
    static func t2() -> UIFont {
        return UIFont(name: "Raleway-Bold", size: 35)!
    }
    
    static func t3() -> UIFont {
        return UIFont(name: "Raleway-Regular", size: 19)!
    }
    
    static func t4() -> UIFont {
        return UIFont(name: "Raleway-Light", size: 20)!
    }
    
    static func t5() -> UIFont {
        return UIFont(name: "Raleway-Bold", size: 18)!
    }
    
    static func t6() -> UIFont {
        return UIFont(name: "Raleway-Light", size: 13)!
    }
    
    static func t7() -> UIFont {
        return UIFont(name: "Raleway-Black", size: 20)!
    }
    
    static func t8() -> UIFont {
        return UIFont(name: "Raleway-Regular", size: 16)!
    }
    
    static func t9() -> UIFont {
        return UIFont(name: "Raleway-Bold", size: 25)!
    }
    
    static func t10() -> UIFont {
        return UIFont(name: "Raleway-Light", size: 14)!
    }
    
    static func t11() -> UIFont {
        return UIFont(name: "Raleway-Black", size: 16)!
    }
    
    static func t12() -> UIFont {
        return UIFont(name: "Raleway-Bold", size: 30)!
    }
    
    static func t13() -> UIFont {
        return UIFont(name: "Raleway-Bold", size: 16)!
    }
    
    static func t14() -> UIFont {
        return UIFont(name: "Raleway-Bold", size: 22)!
    }
    
    static func t15() -> UIFont {
        return UIFont(name: "Raleway-Light", size: 17)!
    }
    
    static func t16() -> UIFont {
        return UIFont(name: "Raleway-Regular", size: 17)!
    }
   
    static func t17() -> UIFont {
        return UIFont(name: "Raleway-Light", size: 10)!
    }
    
    static func t18() -> UIFont {
        return UIFont(name: "Raleway-Bold", size: 14)!
    }
    
    static func t19() -> UIFont {
        return UIFont(name: "Raleway-Regular", size: 13)!
    }
    
    static func t20() -> UIFont {
        return UIFont(name: "Raleway-Medium", size: 10)!
    }
    
    static func t21() -> UIFont {
        return UIFont(name: "Raleway-Regular", size: 14)!
    }
    
    static func nex_t17() -> UIFont {
        return UIFont(name: "Raleway-Bold", size: 14)!
    }
    
    static func nex_t18() -> UIFont {
        return UIFont(name: "Raleway-Regular", size: 13)!
    }
    
    static func nex_t19() -> UIFont {
        return UIFont(name: "Raleway-Bold", size: 13)!
    }
    
    static func nex_t20() -> UIFont {
        return UIFont(name: "Raleway-Medium", size: 16)!
    }
    
    static func nex_t21() -> UIFont {
        return UIFont(name: "Raleway-Medium", size: 12)!
    }
    
    static func lobster(size: CGFloat = 14.0) -> UIFont {
        return UIFont(name: "Lobster-Regular", size: size)!
    }
    
    static func ralewayBlack(size: CGFloat) -> UIFont {
        return UIFont(name: "Raleway-Black", size: size)!
    }
    
    static func ralewayExtraBold(size: CGFloat) -> UIFont {
        return UIFont(name: "Raleway-ExtraBold", size: size)!
    }
    
    static func ralewaySemiBold(size: CGFloat) -> UIFont {
        return UIFont(name: "Raleway-SemiBold", size: size)!
    }
    
    static func ralewayBold(size: CGFloat) -> UIFont {
        return UIFont(name: "Raleway-Bold", size: size)!
    }
    
    static func ralewayMedium(size: CGFloat) -> UIFont {
        return UIFont(name: "Raleway-Medium", size: size)!
    }

    static func ralewayMediumItalic(size: CGFloat) -> UIFont {
        return UIFont(name: "Raleway-MediumItalic", size: size)!
    }
    
    static func ralewayItalic(size: CGFloat) -> UIFont {
        return UIFont(name: "Raleway-Italic", size: size)!
    }
    
    static func raleway(size: CGFloat) -> UIFont {
        return UIFont(name: "Raleway-Regular", size: size)!
    }
    
    static func bwModelicaBoldUltraCondensed(size: CGFloat) -> UIFont {
        return UIFont(name: "BwModelica-BoldUltraCondensed", size: size)!
    }
    
    static func dinPro(size: CGFloat) -> UIFont {
        return UIFont(name: "DINPro-Regular", size: size)!
    }
    
    static func dinProBold(size: CGFloat) -> UIFont {
        return UIFont(name: "DINPro-Bold", size: size)!
    }

    
    static func printFonts() {
        let fontFamilyNames = UIFont.familyNames
        for familyName in fontFamilyNames {
            //BSTLogger.shared.debug("------------------------------")
            //BSTLogger.shared.debug("Font Family Name = [\(familyName)]")
            //let names = UIFont.fontNames(forFamilyName: familyName as String)
            //BSTLogger.shared.debug("Font Names = [\(names)]")
        }
    }
}

