//
//  LandingViewController.swift
//  Airloc
//
//  Created by Wan Rahafiz Wan Abdul Rahim on 26/02/2020.
//  Copyright © 2020 wanrahafiz. All rights reserved.
//

import UIKit
import Library

class LandingViewController: UIViewController {

    
    @IBOutlet weak var tableView: UITableView!
    
    let viewModel: LandingViewModelType = LandingViewModel()
    var dataSource = LandingViewControllerDS()
    
    fileprivate var cellHeightsDict = [IndexPath: CGFloat]()
    
    let addButton = UIButton()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        setupListener()
        setupNavBar()
        
    }
    
    private func setupView() {
        
        tableView.estimatedRowHeight = UITableView.automaticDimension
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none

    }
    
    private func setupNavBar() {
        
    }

    
    func setupListener() {
        
        tableView.registerCellNibForClass(LandingTVC.self)
        tableView.dataSource = dataSource
        tableView.delegate = self
        
        
        viewModel.outputs.collection.bind { [weak self] value in
            guard let strongSelf = self else { return }
            guard let value = value else { return }
            
            DispatchQueue.main.async {
                strongSelf.dataSource.set(welcome: value)
                strongSelf.tableView.reloadData()
                strongSelf.tableView.layoutIfNeeded()
            }
        }
    }
    
    func goToHomePage() {
        let identifier = String(describing: HomeVC.self)
        guard let vc = vc("Main", identifier: identifier) as? HomeVC else { return }
        
        navigationController?.pushViewController(
            vc,
            animated: true
        )
    }
    
}

//MARK: Tableview Cell Delegate
extension LandingViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if let height = cellHeightsDict[indexPath] {
            return height
        }
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeightsDict[indexPath] = cell.frame.height
        
        if let cell = cell as? LandingTVC {
            cell.delegate = self
        }
        
    }
}

//MARK: Landing Tableview Cell Delegate
extension LandingViewController: LandingTVCDelegate {
    func landingTVC(_ cell: LandingTVC) {
        goToHomePage()
    }
    
}
