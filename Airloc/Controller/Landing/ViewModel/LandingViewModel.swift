//
//  LandingViewModel.swift
//  Geofence
//
//  Created by Wan Rahafiz Wan Abdul Rahim on 23/10/2019.
//  Copyright © 2019 Wan Rahafiz Wan Abdul Rahim. All rights reserved.
//

import Foundation

protocol LandingViewModelViewModelInputs {
  
}

protocol LandingViewModelOutputs {
   
    var collection: Box<LandingTVCViewModelType?> { get }
}

protocol LandingViewModelType {
    var inputs: LandingViewModelViewModelInputs { get }
    var outputs: LandingViewModelOutputs { get }
}

class LandingViewModel: LandingViewModelViewModelInputs, LandingViewModelOutputs, LandingViewModelType {
   
    let collection: Box<LandingTVCViewModelType?> = Box(nil)
    
    var inputs: LandingViewModelViewModelInputs { return self }
    var outputs: LandingViewModelOutputs { return self }
    
    init() {
       // Data layout injection 
        let tempWelcomeCollection = LandingTVCViewModel()
        var tempWelcomeCollections = [LandingCVCellViewModel]()
        let welcomeCollection = LandingCVCellViewModel()
        welcomeCollection.outputs.animationName.value = "technology-application"
        welcomeCollection.outputs.description.value = "Plan your trip by checking the nearby airports arround your location"

        tempWelcomeCollections.append(welcomeCollection)

        tempWelcomeCollection.outputs.welcomeCollection.value = tempWelcomeCollections
        collection.value = tempWelcomeCollection
    }
    
}
