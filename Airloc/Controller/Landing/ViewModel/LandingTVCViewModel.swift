//
//  LandingTVCViewModel.swift
//  Geofence
//
//  Created by Wan Rahafiz Wan Abdul Rahim on 23/10/2019.
//  Copyright © 2019 Wan Rahafiz Wan Abdul Rahim. All rights reserved.
//

import Foundation
import UIKit

protocol LandingTVCViewModelInputs {
    
}

protocol LandingTVCViewModelOutputs {
    
    var welcomeCollection: Box<[LandingCVCellViewModelType]?> { get }
    
    var sectionInset: Box<UIEdgeInsets> { get }
    var itemSpacing: Box<CGFloat> { get }
    
    var currentIndex: Box<Int?> { get }

    
}

protocol LandingTVCViewModelType {
    var inputs: LandingTVCViewModelInputs { get }
    var outputs: LandingTVCViewModelOutputs { get }
}

class LandingTVCViewModel: LandingTVCViewModelInputs, LandingTVCViewModelOutputs, LandingTVCViewModelType {
    
    
    let welcomeCollection: Box<[LandingCVCellViewModelType]?> = Box(nil)
    
    let sectionInset: Box<UIEdgeInsets> = Box(UIEdgeInsets(top: 16, left: 24, bottom: 16, right: 24))
    let itemSpacing: Box<CGFloat> = Box(8.0)
    
    let currentIndex: Box<Int?> = Box(nil)

    
    var inputs: LandingTVCViewModelInputs { return self }
    var outputs: LandingTVCViewModelOutputs { return self }
    
    init() {
        
    }
    
}
