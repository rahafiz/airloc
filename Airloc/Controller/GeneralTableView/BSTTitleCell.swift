//
//  BSTTitleCell.swift
//  boostapp
//
//  Created by Cheah Bee Kim on 1/8/18.
//  Copyright © 2018 BOOST. All rights reserved.
//

import UIKit
import Library

class BSTTitleViewModel {
    var containerViewColor: UIColor = UIColor.clear
    var icon: UIImage?
    var smallLeftIcon: UIImage?
    var smallLeftIconUrl: URL?
    
    var slideIn: Bool = false
    var iconShouldVisible: Bool = false
    
    var contentStyle: ThemeBackgroundStyle = .default
    var title: String?
    var subTitle: String?
    var subTitleAttributed: NSAttributedString?
    var titleFont: UIFont?
    var subTitleFont: UIFont?
    var titleColor: UIColor?
    var subTitleColor: UIColor?
    
    var marginTopIconImgView: CGFloat = 60
    var marginBtmIconImgView: CGFloat = 25
    
    var marginBtmTitleLbl: CGFloat = 8
    
    var marginLeft: CGFloat = 20
    var marginRight: CGFloat = 20
    var marginBtm: CGFloat = 0
    
    var marginRightSmallLeftIcon: CGFloat = 16
    var showWidthIconPrio: UILayoutPriority = UILayoutPriority.defaultLow
    var hideWidthIconPrio: UILayoutPriority = UILayoutPriority.defaultHigh
    
    var backgroundColor: UIColor?
}

class BSTTitleCell: UITableViewCell {
    
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var iconContainer: UIView!
    @IBOutlet weak var iconImgView: UIImageView!
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var subTitleLbl: UILabel!
    
    @IBOutlet weak var smallLeftIconImgView: UIImageView!
    @IBOutlet weak var csWidthSmallLeftIconIV: NSLayoutConstraint!
    @IBOutlet weak var csMarginRightSmallLeftIconIV: NSLayoutConstraint!
    
    @IBOutlet weak var csHeightIconImgView: NSLayoutConstraint!
    @IBOutlet weak var csMarginTopIconImgView: NSLayoutConstraint!
    @IBOutlet weak var csMarginBtmIconImgView: NSLayoutConstraint!
    @IBOutlet weak var csMarginBtmTitleLbl: NSLayoutConstraint!
    
    @IBOutlet weak var csMarginBtm: NSLayoutConstraint!
    @IBOutlet weak var csMarginLeft: NSLayoutConstraint!
    @IBOutlet weak var csMarginRight: NSLayoutConstraint!
    
    let viewModel: BSTTitleViewModel = BSTTitleViewModel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        titleLbl.text = ""
        subTitleLbl.text = ""
        
        self.csMarginTopIconImgView.constant = viewModel.marginTopIconImgView
        self.csMarginBtmIconImgView.constant = viewModel.marginBtmIconImgView
        self.csMarginBtmTitleLbl.constant = 0
        self.csHeightIconImgView.priority = UILayoutPriority.defaultLow
    }
    
    // MARK: - Getter
    override open var intrinsicContentSize: CGSize {
        return contentView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
    }
    
    // MARK: - Setup
    func setupView() {
        selectionStyle = .none
        
        ThemeManager.apply(label: titleLbl,
                           style: .bst_t1)
        ThemeManager.apply(label: subTitleLbl,
                           style: .bst_st1)
    }
    
    func slideUpTitle() {
        if viewModel.iconShouldVisible {
            self.csMarginTopIconImgView.constant = viewModel.marginTopIconImgView
            self.csMarginBtmIconImgView.constant = viewModel.marginBtmIconImgView
            self.csHeightIconImgView.priority = UILayoutPriority.defaultLow
        } else {
            self.csMarginTopIconImgView.constant = 10
            self.csMarginBtmIconImgView.constant = 0
            self.csHeightIconImgView.priority = UILayoutPriority.defaultHigh
        }
    }
    
    static func calculateCellSize() -> CGSize {
        let bstTitleCellIdentifier = String(describing: BSTTitleCell.self)
        let bstTitleCellNib = UINib.init(nibName: bstTitleCellIdentifier,
                                                bundle: nil)
        let titleCellTVC = bstTitleCellNib.instantiate(withOwner: self, options: nil).first as? BSTTitleCell
        let widthCell = UIScreen.main.bounds.width
        titleCellTVC?.frame = CGRect(x: 0, y: 0, width: widthCell, height: 0)
        
        let titleVM = BSTTitleViewModel()
        titleVM.title = "test1"
        
        titleCellTVC?.configureWith(value: titleVM)
        titleCellTVC?.setNeedsDisplay()
        titleCellTVC?.layoutIfNeeded()
        titleCellTVC?.layoutSubviews()
        
        let cellSize = CGSize.init(width: titleCellTVC?.intrinsicContentSize.width ?? 0, height: titleCellTVC?.intrinsicContentSize.height ?? 0)
        
        return cellSize
    }
}

extension BSTTitleCell: ValueCell {
    func configureWith(value: BSTTitleViewModel) {
        viewModel.containerViewColor = value.containerViewColor
        viewModel.contentStyle = value.contentStyle
        viewModel.title = value.title
        viewModel.subTitle = value.subTitle
        viewModel.icon = value.icon
        viewModel.iconShouldVisible = value.iconShouldVisible
        viewModel.marginTopIconImgView = value.marginTopIconImgView
        viewModel.marginBtmIconImgView = value.marginBtmIconImgView
        viewModel.marginBtmTitleLbl = value.marginBtmTitleLbl
        viewModel.slideIn = value.slideIn
        viewModel.marginLeft = value.marginLeft
        viewModel.marginRight = value.marginRight
        viewModel.marginBtm = value.marginBtm
        viewModel.titleFont = value.titleFont
        viewModel.subTitleFont = value.subTitleFont
        viewModel.titleColor = value.titleColor
        viewModel.subTitleColor = value.subTitleColor
        viewModel.subTitleAttributed = value.subTitleAttributed
        viewModel.smallLeftIcon = value.smallLeftIcon
        viewModel.smallLeftIconUrl = value.smallLeftIconUrl
        viewModel.backgroundColor = value.backgroundColor
        
        //
        ThemeManager.apply(label: titleLbl,
                           style: .bst_t1,
                           backgroundStyle: viewModel.contentStyle)
        ThemeManager.apply(label: subTitleLbl,
                           style: .bst_st1,
                           backgroundStyle: viewModel.contentStyle)
        
        titleLbl.text = viewModel.title
        
        contentView.backgroundColor = viewModel.containerViewColor
        
        if let subTitleAttributed = viewModel.subTitleAttributed {
            subTitleLbl.attributedText = subTitleAttributed
            
            if subTitleAttributed.string.count > 0 {
                self.csMarginBtmTitleLbl.constant = viewModel.marginBtmTitleLbl
            }
        } else if let subTitle = viewModel.subTitle {
            subTitleLbl.text = subTitle
            
            if subTitle.count > 0 {
                self.csMarginBtmTitleLbl.constant = viewModel.marginBtmTitleLbl
            }
        }
        
        csMarginTopIconImgView.constant = viewModel.marginTopIconImgView
        iconImgView.image = viewModel.icon
        
        csMarginBtm.constant = viewModel.marginBtm
        csMarginLeft.constant = viewModel.marginLeft
        csMarginRight.constant = viewModel.marginRight
        
        if viewModel.slideIn {
            self.slideIn(duration: 0.7, completionDelegate: self)
            
            if viewModel.icon != nil {
                csMarginBtmIconImgView.constant = viewModel.marginBtmIconImgView
            }
        } else {
            slideUpTitle()
        }
        
        if let _titleFont = viewModel.titleFont {
            titleLbl.font = _titleFont
        }
        
        if let _subTitleFont = viewModel.subTitleFont {
            subTitleLbl.font = _subTitleFont
        }
        
        if let _titleColor = viewModel.titleColor {
            titleLbl.textColor = _titleColor
        }
        
        if let _subTitleColor = viewModel.subTitleColor {
            subTitleLbl.textColor = _subTitleColor
        }

        /*if let smallLeftIconUrl = viewModel.smallLeftIconUrl {
            smallLeftIconImgView.af_setImage(withURL: smallLeftIconUrl, completion: {(response) in
                if let _ = response.value {
                    self.csWidthSmallLeftIconIV.priority = self.viewModel.showWidthIconPrio
                    self.csMarginRightSmallLeftIconIV.constant = self.viewModel.marginRightSmallLeftIcon
                }
            })
        }*/ if let smallLeftIcon = viewModel.smallLeftIcon {
            smallLeftIconImgView.image = smallLeftIcon
            csWidthSmallLeftIconIV.priority = viewModel.showWidthIconPrio
            csMarginRightSmallLeftIconIV.constant = viewModel.marginRightSmallLeftIcon
        } else {
            smallLeftIconImgView.image = nil
            csWidthSmallLeftIconIV.priority = viewModel.hideWidthIconPrio
            csMarginRightSmallLeftIconIV.constant = 0
        }
        
        if let backgroundColor = viewModel.backgroundColor {
            self.containerView.backgroundColor = backgroundColor
        }
    }
}

extension BSTTitleCell: CAAnimationDelegate {
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        
    }
}
