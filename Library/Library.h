//
//  Library.h
//  Library
//
//  Created by Wan Rahafiz Wan Abdul Rahim on 26/02/2020.
//  Copyright © 2020 wanrahafiz. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for Library.
FOUNDATION_EXPORT double LibraryVersionNumber;

//! Project version string for Library.
FOUNDATION_EXPORT const unsigned char LibraryVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Library/PublicHeader.h>


