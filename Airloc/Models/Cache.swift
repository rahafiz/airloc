//
//  Cache.swift
//  Airloc
//
//  Created by Wan Rahafiz Wan Abdul Rahim on 27/02/2020.
//  Copyright © 2020 wanrahafiz. All rights reserved.
//

import Foundation
struct Cache {
    

    static func clear() {
        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
    }
        
    static func setAirports(value: [[String: Any]], keyName: String) {
          UserDefaults.standard.set(value, forKey: keyName)
    }
    
    static func getAirports(keyName: String) -> [[String: Any]]? {
        return UserDefaults.standard.value(forKey: keyName) as? [[String : Any]]
    }
}
