//
//  AirportDetailsVCViewModel.swift
//  Airloc
//
//  Created by Wan Rahafiz Wan Abdul Rahim on 27/02/2020.
//  Copyright © 2020 wanrahafiz. All rights reserved.
//

import Foundation
import UIKit
protocol AirportDetailsVCViewModelInputs {
  
}

protocol AirportDetailsVCViewModelOutputs {
   
    var title: Box<BSTTitleViewModel?> { get }
 
}

protocol AirportDetailsVCViewModelType {
    var inputs: AirportDetailsVCViewModelInputs { get }
    var outputs: AirportDetailsVCViewModelOutputs { get }
}

class AirportDetailsVCViewModel: AirportDetailsVCViewModelInputs, AirportDetailsVCViewModelOutputs, AirportDetailsVCViewModelType {
   
    let title: Box<BSTTitleViewModel?> = Box(nil)

    var inputs: AirportDetailsVCViewModelInputs { return self }
    var outputs: AirportDetailsVCViewModelOutputs { return self }
    
    init() {
        // Data layout injection
        
    }
    
}
