//
//  AirportDetailsVC.swift
//  Airloc
//
//  Created by Wan Rahafiz Wan Abdul Rahim on 27/02/2020.
//  Copyright © 2020 wanrahafiz. All rights reserved.
//

import UIKit

class AirportDetailsVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    let viewModel: AirportDetailsVCViewModelType = AirportDetailsVCViewModel()
    var dataSource = AirportDetailsVCDS()
    var airportTitle: String?
    var airportSubtitle: String?

    fileprivate var cellHeightsDict = [IndexPath: CGFloat]()

    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        setupListener()
        setupNavBar()

    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        
    }
    
    private func setupView() {
        automaticallyAdjustsScrollViewInsets = false
        tableView.estimatedRowHeight = UITableView.automaticDimension
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none
        
        // Do any additional setup after loading the view.
        let tempTitleVM = BSTTitleViewModel()
        tempTitleVM.title = airportTitle ?? "Airport Name"
        tempTitleVM.titleColor = UIColor("#3da5e7")
        tempTitleVM.titleFont = UIFont.ralewayBold(size: 24)
        tempTitleVM.subTitle = "Airport distance from your location: \(airportSubtitle ?? "")"
        tempTitleVM.subTitleColor = UIColor("#636363")
        tempTitleVM.subTitleFont = UIFont.raleway(size: 14)
        tempTitleVM.marginTopIconImgView = 16

        viewModel.outputs.title.value = tempTitleVM
          
    }
      
    private func setupNavBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.view.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.layoutIfNeeded()
        self.addBackButtonInNavItem(barStyle: .default)

    }

      
    func setupListener() {
          
        tableView.dataSource = dataSource
        tableView.delegate = self
        dataSource.registerClasses(tableView: tableView)
          
          
        viewModel.outputs.title.bind{ [weak self] value in
            DispatchQueue.main.async {
                guard let strongSelf = self else {return}
                strongSelf.dataSource.set(title: value)
                strongSelf.tableView.reloadData()
                strongSelf.tableView.layoutIfNeeded()
            }
        }
          
      }

}

//MARK: Tableview Cell Delegate
extension AirportDetailsVC: UITableViewDelegate {
      func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if let height = cellHeightsDict[indexPath] {
            return height
        }
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.selectionStyle = .none
        cellHeightsDict[indexPath] = cell.frame.height
        
    }
}

