//
//  AirportDetailsVCDS.swift
//  Airloc
//
//  Created by Wan Rahafiz Wan Abdul Rahim on 27/02/2020.
//  Copyright © 2020 wanrahafiz. All rights reserved.
//

import Foundation
import Library
import UIKit

class AirportDetailsVCDS: ValueCellDataSource {
    enum Section: Int {
        case Title
    }
        
    func set(title: BSTTitleViewModel?) {
        let section = Section.Title.rawValue
               
        self.clearValues(section: section)
          
        if let title = title {
            self.set(
                values: [title],
                cellClass: BSTTitleCell.self,
                inSection: section
            )
        }
    }
        
    override func registerClasses(tableView: UITableView?) {
        tableView?.registerCellNibForClass(BSTTitleCell.self)

    }
    
    override func configureCell(tableCell cell: UITableViewCell, withValue value: Any) {
        switch (cell, value) {
        case let (cell as BSTTitleCell, value as BSTTitleViewModel):
            cell.configureWith(value: value)
        default:
            assertionFailure("")
        }
    }}
